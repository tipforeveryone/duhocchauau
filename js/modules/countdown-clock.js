
//Chỉnh sửa theo thứ tự: Giờ, Phút, Ngày, Tháng, Năm
var target_date = [0,0,21,9,2018]

target_date = new Date(
    target_date[4],
    target_date[3] - 1, //Lưu ý: Chỉ số tháng là zero based nên phải trừ đi 1
    target_date[2],
    target_date[0],
    target_date[1]
);

//Bỏ comment bên dưới để sử dụng count down dựa vào thời gian hiện tại + một khoảng thời gian nào đó (tính theo giờ)
//var so_gio = 12
//var target_date = new Date().getTime() + (1000*3600*so_gio); // set the countdown date
var days, hours, minutes, seconds; // variables for time units

var countdown = document.getElementById("tiles"); // get tag element

getCountdown();

setInterval(function () { getCountdown(); }, 1000);

function getCountdown(){

	// find the amount of "seconds" between now and target
	var current_date = new Date().getTime();
	var seconds_left = (target_date - current_date) / 1000;

	days = pad( parseInt(seconds_left / 86400) );
    days = "<span class='number'>" + days + "</span><span class='label'>ngày</span>";
	seconds_left = seconds_left % 86400;

	hours = pad( parseInt(seconds_left / 3600) );
    hours = "<span class='number'>" + hours + "</span><span class='label'>giờ</span>"
	seconds_left = seconds_left % 3600;

	minutes = pad( parseInt(seconds_left / 60) );
    minutes = "<span class='number'>" + minutes + "</span><span class='label'>phút</span>"
	seconds = pad( parseInt( seconds_left % 60 ) );
    seconds = "<span class='number'>" + seconds + "</span><span class='label'>giây</span>"

	// format countdown string + set tag value
	countdown.innerHTML = "<span>" + days + "</span><span>" + hours + "</span><span>" + minutes + "</span><span>" + seconds + "</span>";
}

function pad(n) {
	return (n < 10 ? '0' : '') + n;
}
