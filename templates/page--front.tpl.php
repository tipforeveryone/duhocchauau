<!-- Banner desktop -->
<div class="standard-block banner">
    <?php $block = module_invoke('views', 'block_view', 'link-block_1'); print render($block['content']); ?>
</div>

<!-- Menu chính -->
<div class="standard-block menuchinh">
    <div class="mobile-display">
        <div class="toggle-menu"></div>
    </div>
    <?php $block = module_invoke('block', 'block_view', '7'); print render($block['content']); ?>
</div>

<div class="standard-block messages">
    <div class="inner">
        <?php print $messages; ?>
    </div>
</div>

<!-- Giới thiệu sự kiện -->
<div class="internal-link"><a name="gioithieu"></a></div>
<div class="standard-block video">
    <h1 class="block-title">Giới thiệu sự kiện</h1>
    <div class="inner">
        <?php $block = module_invoke('block', 'block_view', '3'); print render($block['content']); ?>
    </div>
</div>
<div class="standard-block gioithieu">
    <div class="inner">
        <?php $block = module_invoke('block', 'block_view', '4'); print render($block['content']); ?>
    </div>
</div>

<!-- Số liệu thống kê -->
<div class="standard-block solieu">
    <div class="inner">
        <?php $block = module_invoke('views', 'block_view', 'link-block_4'); print render($block['content']); ?>
    </div>
</div>

<!-- Các hoạt động nổi bật -->
<div class="internal-link"><a name="hoatdong"></a></div>
<div class="standard-block hoatdong">
    <h1 class="block-title">Các hoạt động nổi bật</h1>
    <div class="inner">
        <?php $block = module_invoke('views', 'block_view', 'link-block'); print render($block['content']); ?>
    </div>
</div>

<!-- Hội thảo du học Châu Âu -->
<div class="internal-link"><a name="hoithao"></a></div>
<div class="standard-block hoithao">
    <h1 class="block-title">Hội thảo du học Châu Âu</h1>
    <div class="inner-wrap">
        <div class="inner">
            <?php $block = module_invoke('block', 'block_view', '5'); print render($block['content']); ?>
        </div>
    </div>
</div>

<!-- Phần quà tài trợ -->
<div class="standard-block phanqua">
    <p>Đăng ký tham gia để có cơ hội được nhận QUÀ TẶNG MAY MẮN đặc biệt từ chương trình: </p>
    <div class="inner-wrap">
        <div class="inner">
            <?php $block = module_invoke('views', 'block_view', 'link-block_5'); print render($block['content']); ?>
        </div>
    </div>
</div>

<!-- Thời gian & địa điểm -->
<div class="internal-link"><a name="thoigian"></a></div>
<div class="standard-block thoigian">
    <h1 class="block-title">Thời gian & Địa điểm</h1>
    <div class="inner">
        <?php $block = module_invoke('views', 'block_view', 'link-block_3'); print render($block['content']); ?>
    </div>
</div>

<!-- Đêm ngược đến sự kiện -->
<div class="standard-block countdown">
    <div class="inner">
        <?php //$block = module_invoke('jquery_countdown_timer', 'block_view', 'jquery_countdown_timer'); print render($block['content']); ?>
        <div id="countdown">
            <div id='tiles'></div>
            <script src="http://duhocchauau.vn/sites/all/themes/bootstrapsub/js/modules/countdown-clock.js"></script>
        </div>
    </div>
</div>

<!-- Form đăng ký và thông tin liên hệ -->
<div class="internal-link"><a name="dangky"></a></div>
<div class="standard-block dangky">
    <div class="inner">
        <div class="formdangky">
            <?php
                $block = module_invoke('webform', 'block_view', 'client-block-24');
                print render($block['content']);
                //print theme('block',$block);
                //print render($page['signupform']);
            ?>
        </div>
        <div class="lienhe">
            <?php $block = module_invoke('views', 'block_view', 'article-block_2'); print render($block['content']); ?>
        </div>
    </div>
</div>

<!-- Các nước tham gia -->
<div class="standard-block countries">
    <h1 class="block-title">Các nước tham gia triển lãm</h1>
    <div class="inner">
        <?php $block = module_invoke('block', 'block_view', '6'); print render($block['content']); ?>
    </div>
</div>





<!-- Copyright -->
<div class="standard-block copyright">
    <div class="inner">
        <?php $block = module_invoke('block', 'block_view', '9'); print render($block['content']); ?>
    </div>
</div>

<!-- Nút đăng ký chạy -->
<div class="nutdangky">
    <?php $block = module_invoke('block', 'block_view', '8'); print render($block['content']); ?>
</div>
